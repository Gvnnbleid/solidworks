﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SolidWorks.Interop.sldworks;

namespace SolidWorks
{
    internal static class Helper
    {
        internal static string[] GetCADFilesFromDirectory(string directory)
        {
            if (!Directory.Exists(directory)) return null;

            string[] parts = Directory.GetFiles(directory, "*.sldprt");
            return parts;
        }

        internal static bool ReplaceCustomPropertyValue(CustomPropertyManager customPropertyManager, string customProperty, string replaceable, string replacing)
        {
            string val = customPropertyManager.Get(customProperty);
            if (!string.IsNullOrEmpty(val))
            {
                if (val.ToLower().Contains(replaceable.ToLower()))
                {
                    string newVal = val.Replace(replaceable, replacing);
                    int ret = customPropertyManager.Set(customProperty, newVal);
                    if (ret == 0) return true;
                    else return false;
                }
            }
            return false;
        }


    }
}
